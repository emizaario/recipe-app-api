# recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

## Environment Variables

 * `LISTEN_PORT` - Port to listen on (default: `800`)
 * `APP_HOST` - Hostname of the app to forwar request to (default: `app`)
 * `APP_PORT` -Port of the app to fordward request to (default: `9000`)